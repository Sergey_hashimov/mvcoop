<div class="col-md-12">
    <h1><?= $goods['title'] ?></h1>
    <div class="product-desc">
        <table>
            <tr>
                <td>Характеристика</td>
                <td>Значение</td>
            </tr>

            <tr>
                <td>количество</td>
                <td><?= $goods['stock'] ?></td>
            </tr>

            <tr>
                <td>производитель</td>
                <td><?= $goods['producer'] ?></td>
            </tr>
        </table>
        <div class="photo">
        <img src="<?=$goods['foto']?>" />
        </div>
    </div>
    <hr>
    <div class="product-price">Цена: <?= $goods['price'] ?></div>
    <hr>
    <a type="button" href="/goods/index/category/<?= $goods['cat_id'] ?>" class="btn btn-primary"><?= $goods['category'] ?></a>

</div>