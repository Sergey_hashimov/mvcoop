<?php

class goodsModel extends baseModel{

    public function getGoodsList()
    {
        $query_text = "
                SELECT
                products.id,
                products.foto as foto,
                products.title,
                categories.title AS category,
                producers.title AS producer,
                prices.price,
                products.stock
                FROM products
                JOIN categories ON products.category = categories.id
                JOIN producers ON products.producer = producers.id
                JOIN prices ON products.id = prices.product_id
                WHERE prices.date_till IS NULL
            ";
        $stmt = $this->mysqli->query($query_text);

        while($row = $stmt->fetch_assoc() ){
            $res[] = $row;
        }
        return $res;
    }
    public function getPositionById($id)
    {
        $query_text = "
                SELECT
                products.id,
                products.foto as foto,
                products.title,
                categories.title AS category,
                producers.title AS producer,
                prices.price,
                products.stock,
                categories.id AS cat_id
                FROM products
                JOIN categories ON products.category = categories.id
                JOIN producers ON products.producer = producers.id
                JOIN prices ON products.id = prices.product_id
                WHERE prices.date_till IS NULL
                AND products.id = {$id}
            ";
        $stmt = $this->mysqli->query($query_text);

        $res = $stmt->fetch_assoc();
        return $res;
    }
}
?>
