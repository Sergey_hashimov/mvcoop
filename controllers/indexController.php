<?php
	class indexController extends baseController{
		
		public function __construct(){
			parent::__construct();
		}
		public function index(){

				$this->load->model('menu');

				$vars['title'] = 'Dynamic title';
				$vars['menu'] = $this->menu->getEntries();
				$this->load->view('index',$vars);	
		}

	}
