<?php
class goodsController extends baseController{

    public function __construct(){
        parent::__construct();
    }
    public function index(){

        $this->load->model('goods');

        $vars['title'] = 'Goods list';
        $vars['keywords'] = 'купить киев, электроника, дешево';
        $vars['goods'] = $this->goods->getGoodsList();
        $this->load->view('goods',$vars);
    }
    public function show($id){

        $this->load->model('goods');

        $data['title'] = 'info about product';
        $data['goods'] = $this->goods->getPositionById($id);
        $this->load->view('position',$data);
    }
}

?>